# Zenit-support DEMO
  
Zenit Support is a multi language support service for Zenit FC. It can be embedded through web component insertion.  
  
  
```html
<a href="javascript: void(0)" onclick="document.querySelector('ze-support-popup').open()">!!! PUSH ME !!!</a>
<ze-support-popup
    lang="en"
    user-name="John Smith"
    user-email="johnsmith@test.dev"
    user-agreement="true"
></ze-support-popup>
<script src="#HOST#/local/assets/dist/ze-support-popup.js"></script>
```

Data like language and form fields (Name, Email, checkbox activity) can be passed through web component attributes. 
Files can be upload either by choosing from dialog or by dragging in special area.

Demo URL - http://290569-cu86702.tmweb.ru:4040/
  
Real Project URL - https://fc-zenit.ru/
  
Backend:   
*  Docker  
*  Nginx  
*  PHP 7.2  
*  MySql 5.7  
*  Administration with CMS 1C-Bitrix  
  
Frontend:  
*  Vue.CLI  
*  Vue.JS + TypeScript  
*  Jest  
*  CSS BEM  
  
CI/CD:  
*  Bitbucket Pipelines  
*  ./demo-setup.sh - Process new commit  
