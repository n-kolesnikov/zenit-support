const supportOrigin = new URL(document.currentScript.src).origin;
window.customElements.define('ze-support-popup', class extends HTMLElement {
  template (url) {
    const template = document.createElement('template');
    template.innerHTML = `
      <style>
        :host {
          opacity: 0;
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background: white;
          pointer-events: none;
          /*transition: opacity .33s ease-in-out;*/
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          justify-content: center;
          align-content: flex-start;
          font-size: 16px;
          color: #2c2c2c;
          overflow: auto;
          text-align: left;
      }
      :host(.visible) {
          opacity: 1;
          pointer-events: all;
          z-index: 9999;
      }
      iframe {
        width: 100%;
        height: 100%;
      }
      </style><iframe
          src="${url}"
          name="ze-support"
          frameborder="0"
      />`;
    return template;
  }
  constructor () {
    super();
    this.defaultLang = 'ru';

    this.lang = this.getAttribute('lang');
    this.lang = this.lang || this.defaultLang;

    this.user = {
      name: this.getAttribute('user-name'),
      email: this.getAttribute('user-email'),
      agreement: this.getAttribute('user-agreement')
    }

    this.attachShadow({mode: 'open'});
    this.shadowRoot.appendChild(this.template(this.createUrl()).content.cloneNode(true));
    this._frame = this.shadowRoot.querySelector('iframe');
    // this._frame.addEventListener('load', ()=>{
      // this._frame.contentWindow.postMessage(JSON.stringify(user), supportOrigin);
    // });
    this.visible = false;
  }
  open () {
    this.visible = true;
    this.bodyOverflow = getComputedStyle(document.querySelector('body')).overflow;
    document.querySelector('body').style.overflow = 'hidden';
  }
  close () {
    this.visible = false;
    document.querySelector('body').style.overflow = this.bodyOverflow;
  }
  get visible () { return this.classList.contains('visible') }
  set visible (v) { this.classList.toggle('visible', v) }
  connectedCallback(){
    window.addEventListener('message', (e) => {
      if (e.origin !== supportOrigin) {
        return;
      }
      if (e.data === 'event.close') {
        this.close();
      }
    }, false);
  }
  createUrl(){
    const url = new URL(supportOrigin + '/local/assets/dist/');
    // const url = new URL(supportOrigin + '/');
    url.searchParams.append('origin', location.origin);
    url.searchParams.append('lang', this.lang);
    url.searchParams.append('user[name]', this.user.name);
    url.searchParams.append('user[email]', this.user.email);
    url.searchParams.append('user[agreement]', this.user.agreement);
    return url.toString();
  }
})
