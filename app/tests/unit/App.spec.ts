import { shallowMount, Wrapper } from '@vue/test-utils'
import App from '@/components/App.vue'

describe('App.vue', () => {
  const createCmp = (propsData?: object): Wrapper<Vue> => shallowMount(
    App,
    {
      propsData,
      mocks: {
        $t: () => {}
      }
    })
  it('close handlers called', () => {
    // const msg = 'new message'
    const postToParent = jest.fn()
    const resetForm = jest.fn()
    const wrapper = createCmp();
    (wrapper.vm as any).postToParent = postToParent;
    (wrapper.vm as any).resetForm = resetForm
    wrapper.find('button.support-form-popup__close').trigger('click')
    expect(postToParent.mock.calls.length).toBe(1)
    expect(resetForm.mock.calls.length).toBe(1)
  })
})
