<?php

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Zenit\Support\Rest\SupportFormController;

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('zenit.support');
Loader::includeModule('iblock');
Loader::includeModule('highloadblock');

$controller = new SupportFormController();
$command = SupportFormController::createCommand(Context::getCurrent());
$controller->display($controller->call($command));
