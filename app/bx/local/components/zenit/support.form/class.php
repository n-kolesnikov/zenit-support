<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}

use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Error;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Context;
use Bitrix\Main\Engine\Contract\Controllerable;

class SupportFormComponent extends CBitrixComponent implements Controllerable {
    /** @var ErrorCollection $errorsNonFatal */
    protected $errorsNonFatal;
    /** @var ErrorCollection $errorsFatal */
    protected $errorsFatal;
    protected const DEFAULT_ACTION = 'defaultAction';

    /** @var FormIblockRepository */
    public $formIblock;

    public function __construct($component = null)
    {
        parent::__construct($component);
        $this->errorsNonFatal = new ErrorCollection();
        $this->errorsFatal = new ErrorCollection();
    }

    public function onPrepareComponentParams($arParams)
    {
        $this->arParams = parent::onPrepareComponentParams($arParams);

        $this->arParams['IBLOCK_TYPE'] = trim($arParams['IBLOCK_TYPE']);
        if (empty($this->arParams['IBLOCK_TYPE'])) {
            throw new \RuntimeException('IBLOCK_TYPE_IS_NOT_SET');
        }
        $this->arParams['IBLOCK_ID'] = (int)$arParams['IBLOCK_ID'];
        if (!$this->arParams['IBLOCK_TYPE']) {
            throw new \RuntimeException('IBLOCK_ID_IS_NOT_SET');
        }

        $this->arParams['ACTION_VARIABLE'] = trim($arParams['ACTION_VARIABLE']);
        $this->arParams['ACTION_VARIABLE'] = empty($this->arParams['ACTION_VARIABLE']) ? 'action' : $this->arParams['ACTION_VARIABLE'];
        return $this->arParams;
    }
    /**
     * @return array
     */
    public function executeComponent()
    {
        $this->prepareActions();
        $this->prepareResult();
        $this->includeComponentTemplate();
        return $this->arResult;
    }
    public function prepareDeps()
    {
        $this->formIblock = $this->createFormIblockRepository($this->arParams['IBLOCK_ID']);
    }
//    public function createFormIblockRepository($iblockId): FormIblockRepository
//    {
//        return new FormIblockRepository($iblockId);
//    }
    public function prepareResult()
    {
        $this->arResult['ERRORS'] = [];
        foreach ($this->errorsNonFatal as $error){
            $this->arResult['ERRORS'][] = $error->getMessage();
        }
        $this->arResult['ERRORS_FATAL'] = [];
        foreach ($this->errorsFatal as $error){
            $this->arResult['ERRORS_FATAL'][] = $error->getMessage();
        }
        return true;
    }
    public function configureActions(): array
    {
        return [];
    }
    protected function listKeysSignedParameters()
    {
        return [
//            'ROLE',
        ];
    }
    public function prepareActions()
    {
        $action = $this->request->get($this->arParams['ACTION_VARIABLE']) ?? 'default';
        $action = static::camelize($action).'Action';
        if (!method_exists(get_class($this), $action)) {
            $action = static::DEFAULT_ACTION;
        }
        return $this->$action();
    }
    protected function prepareAjaxAnswer(array $data)
    {
        /** @var Error $error */
        foreach ($this->errorsNonFatal as $error){
            $this->arResult['ERRORS'] = $error->getMessage();
        }
        if(!empty($this->arResult['ERRORS'])){
            $data['message'] = implode(',', $this->arResult['ERRORS']);
        }
        return $data;
    }
    public static function camelize(string $string): string
    {
        $words = explode('_', $string);
        return array_reduce(array_slice($words, 1), function ($acc, $value) {
            return $acc . ucfirst($value);
        }, strtolower($words[0]));
    }
    public function defaultAction(){
//        return 123;
//        if($USER->IsAuthorized()) {
//
//        }else{
//            $this->errors->setError(new Error('USER_NOT_AUTHORIZED'));
//        }
    }
}
