<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}

use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Error;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Context;
use Bitrix\Main\Engine\Contract\Controllerable;

class CPullWebrtcComponent extends CBitrixComponent implements Controllerable {
    /** @var ErrorCollection $errors */
    protected $errors;

    protected $errorsNonFatal = [];
    static $defaultAction = 'defaultAction';
    static $roles = ['client', 'specialist'];

    public function onPrepareComponentParams($arParams){
        $this->errors = new \Bitrix\Main\ErrorCollection();
        if(!Loader::includeModule('pull')){
            $this->errors->setError(new Error('MODULE_NOT_INSTALLED'));
        }
        $this->arParams = parent::onPrepareComponentParams($arParams);

        $this->arParams['CONSULTATIONS_IBLOCK_ID'] = (int)$arParams['CONSULTATIONS_IBLOCK_ID'];
        $this->arParams['CONSULTATION_ID'] = (int)$arParams['CONSULTATION_ID'];
        $this->arParams['ROLE'] = in_array($arParams['ROLE'], static::$roles) ? $arParams['ROLE'] : static::$roles[0];
        $this->arParams['ACTION_VARIABLE'] = trim($arParams['ACTION_VARIABLE']);
        return $this->arParams;
    }
	/**
	 * @return array
	 */
	public function executeComponent(){
//        CJSCore::RegisterExt('pullDemoWebrtc', array(
//			'js' => $this->getPath().'/demo_webrtc.js',
//			'lang' => $this->getPath().'/lang/'.LANGUAGE_ID.'/js_demo_webrtc.php',
//			'rel' => array('webrtc')
//		));
////		CJSCore::Init('pullDemoWebrtc');
        $result = null;
        $this->loadData();
        $this->prepareResult();
//        if($this->request->isAjaxRequest()){
//            if(!empty($this->arResult['ERRORS'])){
//                $result['message'] = implode(', ', $this->arResult['ERRORS']);
//            }
//            echo Json::encode($result);
//        }else{
            $this->includeComponentTemplate();
//        }

        $this->arResult['COMPONENT_NAME'] = $this->getName();
        $this->arResult['SIGNED_PARAMS'] = $this->getSignedParameters();
        return $this->arResult;
	}
    protected function prepareResult(){
        $this->arResult['ERRORS'] = [];
        foreach ($this->errors as $error){
            $this->arResult['ERRORS'][] = $error->getMessage();
        }
        return true;
    }
    public function configureActions(){
        return [];
    }
    protected function listKeysSignedParameters(){
        return [
            'CONSULTATIONS_IBLOCK_ID',
            'ONLINE_CONSULTATIONS_SECTION_CODE',
            'CONSULTATION_ID',
            'ROLE',
        ];
    }
    protected function prepareAjaxAnswer(array $data){
        /** @var Error $error */
        foreach ($this->errors as $error){
            $this->arResult['ERRORS'] = $error->getMessage();
        }
        if(!empty($this->arResult['ERRORS'])){
            $data['message'] = implode(',', $this->arResult['ERRORS']);
        }
        return $data;
    }
	/**
     */
	public function loadData(){
        global $USER;
        if(!$USER->IsAuthorized()){
            $this->errors->setError(new Error('USER_NOT_AUTHORIZED'));
            return;
        }

        $itrConsultation = CIBlockElement::GetList(
            [],
            [
                'ID' => $this->arParams['CONSULTATION_ID'],
                'IBLOCK_ID' => $this->arParams['CONSULTATIONS_IBLOCK_ID'],
//                'SECTION_CODE' => $this->arParams['CONSULTATIONS_IBLOCK_ID'],
                'ACTIVE' => "Y",
            ],
            false,
            false
//            ['ID']
        );
        $oConsultation = $itrConsultation->GetNextElement();
        if(!$oConsultation){
            $this->errors->setError(new Error('CONSULTATION_NF'));
            return;
        }
        $this->arResult = $oConsultation->GetFields();
        $this->arResult['PROPERTIES'] = $oConsultation->GetProperties();
        if((int)$this->arResult['PROPERTIES']['SPECIALIST']['VALUE']<=0){
            $this->errors->setError(new Error('CONSULTATION_SPECIALIST_ID_WRONG'));
            return;
        }

        $specialist = CIBlockElement::GetList(
            [],
            [
                'ID' => $this->arResult['PROPERTIES']['SPECIALIST']['VALUE'],
                'IBLOCK_ID' => $this->arResult['PROPERTIES']['SPECIALIST']['LINK_IBLOCK_ID'],
                'ACTIVE' => "Y",
            ],
            false,
            false,
            ['ID', 'PROPERTY_SPECIALIST_ID']
        )->Fetch();

        if(!is_array($specialist)){
            $this->errors->setError(new Error('SPECIALIST_NF'));
            return;
        }
        if((int)$specialist['PROPERTY_SPECIALIST_ID_VALUE']<=0){
            $this->errors->setError(new Error('SPECIALIST_USER_ID_WRONG'));
            return;
        }
        if($this->arParams['ROLE'] == 'specialist'){
            $remoteUserId = $this->arResult['PROPERTIES']['CLIENT_ID']['VALUE'];
        }else{
            $remoteUserId = $specialist['PROPERTY_SPECIALIST_ID_VALUE'];
        }
        $this->arResult['REMOTE_USER'] = CUser::GetByID($remoteUserId)->Fetch();
        $this->arResult['LOCAL_USER'] = CUser::GetByID($USER->getId())->Fetch();
    }
	public function simplePullAction(){
        $this->loadData();
//        $command = =='offer'?'offer':'answer';
//        CPullChannel::CheckOnlineChannel();
//        Bitrix\Main\Diag\Debug::writeToFile($this->arResult['REMOTE_USER']['ID']);
//        Bitrix\Main\Diag\Debug::writeToFile($this->arParams);
//        Bitrix\Main\Diag\Debug::writeToFile($this->request->get('command'));
        PullEvent::add($this->arResult['REMOTE_USER']['ID'], [
            'module_id' => 'zv',
            'command' => $this->request->get('command'),
            'params' => [
                'senderId' => (int)$this->arResult['LOCAL_USER']['ID'],
            ]
        ]);
        return $this->prepareAjaxAnswer([
            'result' => true
        ]);
    }
    public function signalingAction(){
        $this->loadData();
        PullEvent::add($this->arResult['REMOTE_USER']['ID'], [
            'module_id' => 'zv',
            'command' => 'signaling',
            'params' => [
                'senderId' => (int)$this->arResult['LOCAL_USER']['ID'],
                'params' => $this->request->getPost('PARAMS')
            ]
        ]);
    }
    public function jsErrorAction(){
        global $USER;
        $message = [
            'error' => $this->request->get('error'),
            'navigator' => $this->request->get('navigator'),
            'user_id' => $USER->GetID(),
            'cons_id' => $this->arParams['CONSULTATION_ID'],
            'timestamp' => date('c'),
        ];
        Bitrix\Main\Diag\Debug::writeToFile($message);
        bxmail('zveroboy85@gmail.com', 'WEBRTC_COMPONENT_JS_EXCEPTION', print_r($message, 1));
        return $this->prepareAjaxAnswer([
            'sended' => true
        ]);

    }
//    public function appParamsAction(){
//        $this->loadData();
//        $context = Context::getCurrent();
//        $server = $context->getServer();
//        $response['status'] = 'success';
//        $response['postUrl'] = "/local/components/zv/pull.webrtc/call.ajax.php";
//        $pullConfig = CPullChannel::GetConfig($this->arResult['LOCAL_USER']['ID']);
//        $response['wsUrl'] = str_replace('#DOMAIN#', $server->getServerName(), $pullConfig['PATH_WS']);
//        return $this->prepareAjaxAnswer([
//            'result' => true
//        ]);
//    }
//	public function checkOnlineAction(){
//        CPullChannel::CheckOnlineChannel();
//    }
}
