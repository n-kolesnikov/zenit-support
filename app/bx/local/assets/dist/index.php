<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$recaptchaKey = Option::get('zenit.support', 'recaptcha_public_key');

$doc = new DOMDocument();
$doc->loadHTMLFile(__DIR__.'/index.html');
$xpath = new DOMXPath($doc);
$htmlElement = $xpath->query('//html');
$htmlElement[0]->setAttribute('lang', $_GET['lang']);
$appElement = $xpath->query('//div[@id="app"]');
$scheme = 'http' . (Context::getCurrent()->getRequest()->isHttps() ? 's': '');
$appElement[0]->setAttribute('data-host', sprintf('%s://%s', $scheme, $_SERVER['HTTP_HOST']));
$appElement[0]->setAttribute('data-locale', $_GET['lang']);
$appElement[0]->setAttribute('data-user-name', $_GET['user']['name']);
$appElement[0]->setAttribute('data-user-email', $_GET['user']['email']);
$appElement[0]->setAttribute('data-user-agreement', $_GET['user']['agreement']);
$appElement[0]->setAttribute('data-recaptcha-key', $recaptchaKey);
echo $doc->saveHTML();
