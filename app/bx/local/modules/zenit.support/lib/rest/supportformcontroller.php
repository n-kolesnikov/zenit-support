<?php

namespace Zenit\Support\Rest;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\Web\Json;
use Zenit\Support\Command;
use Zenit\Support\MailNotifier;
use Zenit\Support\Recaptcha;
use Zenit\Support\FormIblock;

class SupportFormController
{
    public function __construct()
    {}
    /**
     * @param Command\Command|null $command
     * @return ControllerResult
     */
    public function call(?Command\Command $command = null): ControllerResult
    {
        $result = new ControllerResult();
        try {
            if (!$command ) {
                throw new \Bitrix\Main\NotImplementedException('COMMAND_NOT_FOUND');
            }
            $result->setData($command->execute());
            $result->setStatusSuccess();
        } catch (\Exception $e) {
            $result->addError($e);
        }
        return $result;
    }
    public function display(ControllerResult $result): void
    {
        echo Json::encode($result);
    }

    /**
     * @param Context $context
     * @return Command\Command|null
     * @throws
     */
    public static function createCommand(Context $context): ?Command\Command
    {
        $iblockId = Option::get('zenit.support', 'support_form_iblock_id');
        $recaptchaPrivateKey = Option::get('zenit.support', 'recaptcha_private_key');

        $command = null;
        switch ($context->getRequest()->get('action')) {
            case 'getTopics':
                $command = new Command\GetTopicsCommand(
                    new FormIblock($iblockId),
                    $context->getRequest()->isHttps()?'https':'http',
                    $context->getServer()->get('HTTP_HOST')
                );
                break;
            case 'save':
                $notifier = new MailNotifier('ZENIT_SUPPORT_FORM_ADDED', Context::getCurrent()->getSite());
                $recaptcha = new Recaptcha($recaptchaPrivateKey);
                $command = new Command\SaveCommand(
                    $context->getRequest(),
                    new FormIblock($iblockId),
                    $notifier,
                    $recaptcha
                );
                break;
        }
        return $command;
    }
}
