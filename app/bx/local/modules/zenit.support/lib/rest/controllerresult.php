<?php

namespace Zenit\Support\Rest;

class ControllerResult implements \JsonSerializable
{
    private $errors = [];
    private $data;
    private $status = 'error';
    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'status' => $this->status,
            'data' => $this->data,
            'errors' => $this->errors
        ];
    }
    public function setStatusError(): void
    {
        $this->status = 'error';
    }
    public function addError(Exception $e): void
    {
        $this->setStatusError();
        $this->errors[] = $e->getMessage() . $e->getTraceAsString();
    }
    public function setStatusSuccess(): void
    {
        $this->status = 'success';
    }
    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }
}
