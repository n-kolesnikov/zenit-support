<?php

namespace Zenit\Support;

use Bitrix\Main\Result;
use Bitrix\Main\Mail\Event;

class MailNotifier
{
    /** @var string */
    private $name;
    /** @var string */
    private $siteId;
    public function __construct(string $name, string $siteId)
    {
        $this->name = $name;
        $this->siteId = $siteId;
    }

    /**
     * @param array $messageFields
     * @return Result
     */
    public function send(array $messageFields): Result
    {
        $eventFields = [
            'EVENT_NAME' => $this->name,
            'C_FIELDS' => $messageFields,
            'LID' => $this->siteId
        ];
        return Event::send($eventFields);
    }
}
