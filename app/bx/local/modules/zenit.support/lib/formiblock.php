<?php

namespace Zenit\Support;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Iblock\PropertyTable;
use Zenit\Support\Model;

class FormIblock
{
    /** @var int */
    private $iblockId;
    public function __construct(int $iblockId)
    {
        $this->iblockId = $iblockId;
    }
    /**
     * @param string $propertyCode
     * @param array|null $select
     * @param array $valueIds
     * @return array
     * @throws \Exception
     */
    public function getHighLoadPropertyValues(string $propertyCode, ?array $select = null, ?array $valueIds = null): array
    {
        $property = PropertyTable::getRow(['filter' => ['IBLOCK_ID' => $this->iblockId, 'CODE' => $propertyCode]]);
        if (!is_array($property)) {
            throw new \Exception('PROPERTY_NOT_FOUND');
        }
        if ($property['USER_TYPE'] !== 'directory' || empty($property['USER_TYPE_SETTINGS'])) {
            throw new \Exception('PROPERTY_BAD_ATTRIBUTES');
        }
        $hlClass = $this->getHLClassByTableName($property['USER_TYPE_SETTINGS_LIST']['TABLE_NAME']);

        $result = [];
        $hlGetListParams = [];
        if ($valueIds) {
            $hlGetListParams['filter']['UF_XML_ID'] = $valueIds;
        }
        if ($select) {
            $hlGetListParams['select'] = $select;
        }
        $valuesItr = $hlClass::getList($hlGetListParams);
        while($value = $valuesItr->fetch()){
            $result[$value['UF_XML_ID']] = $value;
        }
        return $result;
    }
    public function saveElement(Model\Form $form): int
    {
        $fields = [
            'IBLOCK_ID' => $this->iblockId,
            'NAME' => $form->email,
            'PREVIEW_TEXT' => $form->message,
            'PROPERTY_VALUES' => [
                'TOPIC' => $form->topicId,
                'NAME' => $form->name,
                'ORDER_ID_OR_EMAIL' => $form->orderIdOrEmail
            ]
        ];
        foreach ($form->files['name'] as $f => $name) {
            $fields['PROPERTY_VALUES']['FILES']["n$f"]['name'] = $name;
            $fields['PROPERTY_VALUES']['FILES']["n$f"]['type'] = $form->files['type'][$f];
            $fields['PROPERTY_VALUES']['FILES']["n$f"]['tmp_name'] = $form->files['tmp_name'][$f];
            $fields['PROPERTY_VALUES']['FILES']["n$f"]['error'] = $form->files['error'][$f];
            $fields['PROPERTY_VALUES']['FILES']["n$f"]['size'] = $form->files['size'][$f];
        }
        $iblockElement = new \CIBlockElement;
        $result = $iblockElement->Add($fields);
        if (!$result) {
            throw new \RuntimeException($iblockElement->LAST_ERROR);
        }
        return $result;
    }
    private function getHLClassByTableName(string $tableName): string
    {
        static $hlClass;
        if (!isset($hlClass)) {
            $hlData = HighloadBlockTable::getRow(['filter' => ['TABLE_NAME' => $tableName]]);
            $hlClass = HighloadBlockTable::compileEntity($hlData)->getDataClass();
        }
        return $hlClass;
    }
}
