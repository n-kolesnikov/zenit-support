<?php

namespace Zenit\Support;

use Bitrix\Main\ArgumentException;

class Recaptcha
{
    private const VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';
    private $secretKey;
    /**
     * @param string $secretKey
     * @throws ArgumentException
     */
    public function __construct(string $secretKey)
    {
        if (empty($secretKey)) {
            throw new ArgumentException('EMPTY_SECRETE_KEY');
        }
        $this->secretKey = $secretKey;
    }

    /**
     * @param string $response
     * @return bool
     * @throws ArgumentException
     */
    public function verify (string $response): bool
    {
        if (empty($response)) {
            throw new ArgumentException('EMPTY_SECRETE_KEY');
        }
        $data = [
            'secret' => $this->secretKey,
            'response' => $response
        ];
        $options = [
            'http' => [
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context  = stream_context_create($options);
        $verify = file_get_contents(self::VERIFY_URL, false, $context);
        $result = json_decode($verify, false);
        if (!$result) {
            throw new \RuntimeException('JSON_PARSE_ERROR');
        }
        return $result->success;
    }
}
