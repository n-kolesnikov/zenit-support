<?php

namespace Zenit\Support\Command;

use Zenit\Support\Model;
use Zenit\Support\FormIblock;

class GetTopicsCommand implements Command
{
    private $scheme;
    private $host;
    private $repository;
    public function __construct(FormIblock $repository, string $scheme, string $host)
    {
        $this->repository = $repository;
        $this->scheme = $scheme;
        $this->host = $host;
    }
    /** @throws */
    public function execute()
    {
        return array_values(array_map(function ($item) {
            $topic = new Model\Topic();
            $topic->id = $item['ID'];
            $topic->xmlId = $item['UF_XML_ID'];
            $topic->optionalProperties = $item['UF_OPT_PROPERTIES'];
            $topic->setLocaleFields('ru', [
                'name' => $item['UF_NAME'],
                'description' => $item['UF_DESCRIPTION'] ?? ''
            ]);
            $topic->setLocaleFields('en', [
                'name' => $item['UF_NAME_EN'],
                'description' => $item['UF_DESCRIPTION_EN'] ?? ''
            ]);
            $src = $this->getFileSrc($item['UF_FILE']);
            $topic->picture = $src ? $this->getUri($src) : null;
            return $topic;
        }, $this->repository->getHighLoadPropertyValues('TOPIC')));
    }
    public function getFileSrc(?int $fileId): ?string
    {
        $file = $fileId ? \CFile::GetFileArray($fileId) : null;
        $src = $file ? \CFile::GetFileSRC($file) : null;
        return !empty($src) ? $src : null;
    }
    public function getUri(string $relativePath):string {
        if (self::isUri($relativePath)) {
            return $relativePath;
        }
        return sprintf(
            '%s://%s%s',
            $this->scheme,
            $this->host,
            $relativePath
        );
    }
    /**
     * @param string $path
     * @return bool
     */
    public static function isUri(string $path):bool{
        return preg_match('/^[a-z]+:\//i', $path);
    }
}
