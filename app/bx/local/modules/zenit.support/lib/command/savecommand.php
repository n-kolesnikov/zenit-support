<?php

namespace Zenit\Support\Command;

use Bitrix\Main\Request;
use Zenit\Support\Model;
use Zenit\Support\FormIblock;
use Zenit\Support\MailNotifier;
use Zenit\Support\Recaptcha;
use RuntimeException;

class SaveCommand implements Command
{
    private $repository;
    private $form;
    private $notifier;
    private $recaptcha;
    public function __construct(Request $request, FormIblock $repository, MailNotifier $notifier, Recaptcha $recaptcha)
    {
        $this->repository = $repository;
        $this->form = new Model\Form();
        $this->form->topicId = $request->getPost('topic_id');
        $this->form->name = $request->getPost('name');
        $this->form->email = $request->getPost('email');
        $this->form->message = $request->getPost('message');
        $this->form->orderIdOrEmail = $request->getPost('orderIdOrEmail');
        $this->form->files = $request->getFile('files');
        $this->form->reCaptcha = $request->getPost('reCaptcha');
        $this->notifier = $notifier;
        $this->recaptcha = $recaptcha;
    }
    /** @throws */
    public function execute()
    {
        if (!$this->recaptcha->verify($this->form->reCaptcha)) {
            throw new RuntimeException('UNABLE_VERIFY');
        }
        $topics = array_filter($this->repository->getHighLoadPropertyValues('TOPIC'), function ($topic) {
            return $topic['UF_XML_ID'] == $this->form->topicId;
        });
        $topic = array_values($topics)[0];
        if (!is_array($topic)) {
            throw new RuntimeException('TOPIC_NOT_FOUND');
        }

        $elementId = $this->repository->saveElement($this->form);
        $sendResult = $this->notifier->send([
            'TOPIC' => $topic['UF_NAME'],
            'ID' => $elementId,
            'NAME' => $this->form->name,
            'EMAIL' => $this->form->email,
            'MESSAGE' => $this->form->message,
            'ORDER_ID_OR_EMAIL' => $this->form->orderIdOrEmail,
        ]);
        if (!$sendResult->isSuccess()) {
            throw new RuntimeException(implode(', ', $sendResult->getErrorMessages()));
        }
        return true;
    }
}
