<?php

namespace Zenit\Support\Command;

interface Command {
    public function execute();
}
