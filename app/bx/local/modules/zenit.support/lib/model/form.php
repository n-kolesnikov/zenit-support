<?php

namespace Zenit\Support\Model;

class Form
{
    public $topicId;
    public $orderIdOrEmail;
    public $name;
    public $email;
    public $message;
    public $files;
    public $reCaptcha;
}
