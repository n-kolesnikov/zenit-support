<?php

namespace Zenit\Support\Model;

class Topic implements \JsonSerializable
{
    public $id;
    public $name;
    public $picture;
    public $xmlId;
    private $localeFields;
    public $optionalProperties;

    /**
     * @param string $locale
     * @param mixed $localeFields
     */
    public function setLocaleFields(string $locale, array $localeFields): void
    {
        $this->localeFields[$locale] = $localeFields;
    }
    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'optionalProperties' => $this->optionalProperties ?: [],
            'localeFields' => $this->localeFields,
            'picture' => $this->picture,
            'xmlId' => $this->xmlId,
        ];
    }
}
