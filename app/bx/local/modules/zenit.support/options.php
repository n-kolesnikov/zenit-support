<?php
/* @global CUser $USER */
/* @global CMain $APPLICATION */
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

$moduleId = 'zenit.support';
Loader::includeModule($moduleId);
Loc::loadMessages(Loader::getDocumentRoot().'/'.Loader::BITRIX_HOLDER."/modules/main/options.php");
Loc::loadMessages(__FILE__);
if (!$USER->CanDoOperation("{$moduleId}_settings")) {
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$arAllOptions = [
	'support_form_iblock_id' => [Loc::getMessage("$moduleId.support_form_iblock_id"), ["number", 6, ' min="0"']],
	'recaptcha_public_key' => [Loc::getMessage("$moduleId.recaptcha_public_key"), ["text", 40]],
	'recaptcha_private_key' => [Loc::getMessage("$moduleId.recaptcha_private_key"), ["text", 40]],
];

$arOptions2Tab = [
	'edit1' => [
		'support_form_iblock_id',
        ['heading', Loc::getMessage("$moduleId.heading_recaptcha")],
        'recaptcha_public_key',
        'recaptcha_private_key',
    ],
];
$aTabs = [
	["DIV" => "edit1", "TAB" => Loc::getMessage('MAIN_TAB_SET'), "ICON" => "{$moduleId}_settings", "TITLE" => Loc::getMessage('MAIN_TAB_TITLE_SET')],
	["DIV" => "edit2", "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"), "ICON" => "{$moduleId}settings", "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")],
];

$simplePrintOptions = function($key) use ($arAllOptions, $arOptions2Tab, $moduleId){
	if(!array_key_exists($key, $arOptions2Tab)){
		return;
	}
	foreach($arOptions2Tab[$key] as $optionCode) {
		if(is_array($optionCode)&&$optionCode[0]=='heading'){
			?>
			<tr class="heading">
				<td colspan="2"><b><?=$optionCode[1]?></b></td>
			</tr>
			<?
			continue;
		}
		$arOption = $arAllOptions[$optionCode];
		$val = Option::get($moduleId, $optionCode, $arOption[2]);
		$type = $arOption[1];
		?>
		<tr>
			<td class="adm-detail-content-cell-l"><?
				if ($type[0] == "checkbox")
					echo "<label for=\"".htmlspecialcharsbx($optionCode)."\">".$arOption[0]."</label>";
				else
					echo $arOption[0];
				?>: </td>
			<td class="adm-detail-content-cell-r"><?
				if($type[0]=="checkbox"):
					?><input type="checkbox" name="<?echo htmlspecialcharsbx($optionCode)?>" id="<?echo htmlspecialcharsbx($optionCode)?>" value="Y"<?if($val=="Y")echo" checked";?> /><?
				elseif ($type[0]=="text"):
					?><input type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($optionCode)?>" /><?
				elseif ($type[0]=="email"):
					?><input type="email" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($optionCode)?>" /><?
				elseif ($type[0]=="number"):
					?><input type="number" size="<?echo $type[1]?>" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($optionCode)?>"<?echo $type[2]?> /><?
				elseif($type[0]=="selectbox"):?>
					<select name="<?echo htmlspecialcharsbx($optionCode)?>"><?
					foreach($type[1] as $optionValue => $optionDisplay){
						?><option value="<?echo $optionValue?>"<?if($val==$optionValue)echo" selected";?>><?echo htmlspecialcharsbx($optionDisplay)?></option><?
					}?></select><?
				elseif($type[0]=="textarea"):
					?><textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($optionCode)?>"><?echo htmlspecialcharsbx($val)?></textarea><?
				elseif($type[0]=="date"):
					echo CalendarDate($optionCode, $val);
				endif;
				?></td>
		</tr>
		<?
	}
};
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
{
	if (strlen($RestoreDefaults) > 0)
	{
		Option::delete($moduleId);

		$z = CGroup::GetList($v1="id",$v2="asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
		while($zr = $z->Fetch()){
			$APPLICATION->DelGroupRight($moduleId, array($zr["ID"]));
		}
	}
	else
	{
		foreach($arAllOptions as $name=>$arOption)
		{
			$val = $_POST[$name];
			if ($arOption[1][0] == "checkbox" && $val != "Y"){
				$val = "N";
			}
			Option::set($moduleId, $name, $val);
		}
	}
}
$tabControl->Begin();
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&amp;lang=<?echo LANG?>" name="<?=$moduleId?>_settings">
	<?=bitrix_sessid_post()?>
	<?php
	$tabControl->BeginNextTab();
	$simplePrintOptions('edit1');
	$tabControl->BeginNextTab();
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
	$tabControl->Buttons();?>
	<script>
		function confirmRestoreDefaults()
		{
			return confirm('<?echo AddSlashes(Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>');
		}
	</script>
	<input type="submit" name="Update" value="<?echo Loc::getMessage("MAIN_SAVE")?>">
	<input type="hidden" name="Update" value="Y">
	<input type="reset" name="reset" value="<?echo Loc::getMessage("MAIN_RESET")?>">
	<input type="submit" name="RestoreDefaults" title="<?echo Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirmRestoreDefaults();" value="<?echo Loc::getMessage("MAIN_RESTORE_DEFAULTS")?>">
	<?$tabControl->End();?>
</form>
