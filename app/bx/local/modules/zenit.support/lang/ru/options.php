<?php
$moduleId = 'zenit.support';
$MESS["$moduleId.support_form_iblock_id"] = 'ID инфоблока формы обращений';
$MESS["$moduleId.heading_recaptcha"] = 'reCaptcha';
$MESS["$moduleId.recaptcha_public_key"] = 'Публичный ключ';
$MESS["$moduleId.recaptcha_private_key"] = 'Приватный ключ';
