<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
class zenit_support extends CModule
{
	const MODULE_ID = 'zenit.support';
	var $MODULE_ID = 'zenit.support';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(__DIR__ . '/version.php');
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("zenit.support_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("zenit.support_MODULE_DESC");

		$this->PARTNER_NAME = Loc::getMessage("zenit.support_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("zenit.support_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		// RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CBitrixLiveapi', 'OnBuildGlobalMenu');
		// RegisterModuleDependences('main', 'OnEpilog', self::MODULE_ID, 'CBitrixLiveapi', 'OnAdminPageLoad');
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		// UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CBitrixLiveapi', 'OnBuildGlobalMenu');
		// UnRegisterModuleDependences('main', 'OnEpilog', self::MODULE_ID, 'CBitrixLiveapi', 'OnAdminPageLoad');
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		return true;
	}

	function UnInstallFiles()
	{

		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
?>
