<?php

$_SERVER["DOCUMENT_ROOT"] = realpath(__DIR__ . '/../../..');

define("LANGUAGE_ID", 'ru');
define("LANG", 's1');
define("STOP_STATISTICS", true);
define("NO_AGENT_CHECK", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
//define("LOG_FILENAME", 'php://stderr');

$GLOBALS['DBType'] = 'mysql';
$_SERVER["SCRIPT_NAME"] = '/';
//$_SERVER['REQUEST_METHOD'] = "cli";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
// Альтернативный способ вывода ошибок типа "DB query error.":
//$GLOBALS["DB"]->debug = true;

// Заменяем вывод фатальных ошибок Битрикса на STDERR - чтобы не было "молчаливого" поведения

class PhpunitFileExceptionHandlerLog extends Bitrix\Main\Diag\FileExceptionHandlerLog {
    public function write($exception, $logType)
    {
        $text = Bitrix\Main\Diag\ExceptionHandlerFormatter::format($exception, false, $this);
        $msg = date("Y-m-d H:i:s")." - Host: ".$_SERVER["HTTP_HOST"]." - ".static::logTypeToString($logType)." - ".$text."\n";
        fwrite(STDERR, $msg);
    }
}

$handler = new PhpunitFileExceptionHandlerLog;

$bitrixExceptionHandler = \Bitrix\Main\Application::getInstance()->getExceptionHandler();

$reflection = new \ReflectionClass($bitrixExceptionHandler);
$property = $reflection->getProperty('handlerLog');
$property->setAccessible(true);
$property->setValue($bitrixExceptionHandler, $handler);