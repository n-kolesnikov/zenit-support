<?php

namespace Zenit\Tests\Components;

use PHPUnit\Framework\TestCase;

class SimpleTest extends TestCase
{
    protected $backupGlobals = false;

//    protected $backupGlobalsBlacklist = ['DB'];
    // Тест подключения модуля

    public function testCmodule()
    {
        $this->assertTrue(\CModule::IncludeModule('iblock'));
        $this->assertFalse(\CModule::IncludeModule('module.not.exists'));
    }

    // Тест на Exception
    public function testInvalidArgumentException()
    {
        $this->expectException('InvalidArgumentException');
        throw new \InvalidArgumentException("Error Processing Request", 1);
    }
}
