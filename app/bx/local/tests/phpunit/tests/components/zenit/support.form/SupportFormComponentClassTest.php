<?php

namespace Zenit\Tests\Components;

use PHPUnit\Framework\TestCase;

class SupportFormComponentClassTest extends TestCase
{
    private const COMPONENT_NAME = 'zenit:support.form';
    /** @var \SupportFormComponent */
    private $component;
    protected $backupGlobals = false;
    //    protected $backupGlobalsBlacklist = ['DB'];

    public static function setUpBeforeClass(): void
    {
        \CBitrixComponent::includeComponentClass(static::COMPONENT_NAME);
    }
    public function setUp(): void
    {
        $this->component = new \SupportFormComponent();
        $this->component->initComponent(static::COMPONENT_NAME);
    }
    public function testDefaultAction()
    {
        $this->assertEquals(123, $this->component->defaultAction());
    }
}
