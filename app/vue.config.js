module.exports = {
  outputDir: 'bx/local/assets/dist',
  publicPath: '/local/assets/dist/',
  pluginOptions: {
    i18n: {
      locale: 'ru',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  },
  configureWebpack: {
    devServer: {
      host: 'client.fc-zenit.local',
      port: '8080'
    }
  }
  // chainWebpack: config => {
  //   config
  //     .plugin('copy')
  //     .tap(([options]) => {
  //       options[0].from += '/local/assets/src'
  //       // options[0].ignore.push('**/*')
  //       return [options]
  //     })
  // }
}
