import { Response } from '@/data/model/Response'
import { SupportFormData, SupportFormTopic } from '@/data/model/SupportForm'
import { json } from '@/web/http'
export default class {
  // eslint-disable-next-line no-useless-constructor
  constructor (protected path: string) {}

  async getTopics (): Promise<Response<SupportFormTopic[]>> {
    const url = new URL(this.path)
    url.searchParams.append('action', 'getTopics')
    // const searchParams = new URLSearchParams()
    // searchParams.append('action', 'getTopics')
    // return json(path, { body: searchParams, method: 'POST' })
    return json(url.toString())
  }

  async saveForm (data: SupportFormData): Promise<Response<SupportFormTopic[]>> {
    const url = new URL(this.path)
    const formData = new FormData()
    url.searchParams.append('action', 'save')
    formData.append('topic_id', data.topic)
    formData.append('name', data.name)
    formData.append('email', data.email)
    formData.append('message', data.message)
    formData.append('orderIdOrEmail', data.orderIdOrEmail)
    formData.append('reCaptcha', data.reCaptcha)
    data.files.forEach((file) => formData.append('files[]', file, file.name))
    return json(url.toString(), { body: formData, method: 'post' })
  }
}
