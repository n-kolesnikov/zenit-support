export type Response<T> = {
    status: 'error'|'success';
    errors: string[];
    data: T;
}
