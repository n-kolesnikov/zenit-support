import { LocaleMessageObject } from 'vue-i18n'
import { Form } from '@/input/Field'

export type SupportFormTopic = {
  id: string;
  localeFields: LocaleMessageObject;
  optionalProperties: string[];
  picture?: string;
  xmlId: string;
}
export type SupportFormData = {
  topic: string;
  orderIdOrEmail: string;
  name: string;
  email: string;
  message: string;
  personalTermsAgreement: boolean;
  marketingTermsAgreement: boolean;
  files: File[];
  reCaptcha: string;
}
export function extractDTO (form: Form): SupportFormData {
  return {
    topic: form.fields.topic.value as string,
    orderIdOrEmail: form.fields.orderIdOrEmail.value as string,
    name: form.fields.name.value as string,
    email: form.fields.email.value as string,
    message: form.fields.message.value as string,
    personalTermsAgreement: form.fields.personalTermsAgreement.value as boolean,
    marketingTermsAgreement: form.fields.marketingTermsAgreement.value as boolean,
    files: form.fields.files.value as File[],
    reCaptcha: form.fields.reCaptcha.value as string
  }
}
