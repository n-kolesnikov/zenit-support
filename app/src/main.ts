import Vue from 'vue'
import App from './components/App.vue'
import { createVueI18n } from './i18n'

Vue.config.productionTip = false

const locale = document.querySelector<HTMLElement>('#app')!.dataset.locale
const i18n = createVueI18n(locale)

const props: {[k: string]: string|boolean|undefined } = { ...document.querySelector<HTMLElement>('#app')!.dataset }
props.userAgreement = props.userAgreement === 'true'
new Vue({
  i18n,
  render: h => h(App, { props })
}).$mount('#app')
