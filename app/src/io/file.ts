export function readFileData (file: File): Promise< FileReader['result'] > {
  return new Promise<FileReader['result']>((resolve) => {
    const reader = new FileReader()
    reader.onload = (e): void => resolve((e.target as FileReader).result)
    reader.readAsDataURL(file)
  })
}
