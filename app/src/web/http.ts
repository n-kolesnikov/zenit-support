export async function json<T> (input: RequestInfo, init?: RequestInit): Promise<T> {
  const response = await fetch(input, init)
  return response.json()
}
