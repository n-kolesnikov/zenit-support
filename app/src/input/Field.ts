import { Validator } from './Validator'

export class Field {
  touched = false;
  constructor (
    public validator: Validator,
    public value: File[]|string[]|string|boolean|null = null
  ) {}

  get validation () {
    return this.validator.validate(this.value)
  }
}

export class Form {
  constructor (public fields: {[key: string]: Field}) {}
  get isValid () {
    return Object.values(this.fields).every((field) => field.validation.length === 0)
  }
}
