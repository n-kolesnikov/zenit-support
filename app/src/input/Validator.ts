import { Field } from '@/input/Field'

export class Validator {
  // eslint-disable-next-line no-useless-constructor
  constructor (private rules: Rule[] = []) {}
  validate (value: Field['value']): string[] {
    return this.rules
      .filter((rule) => !rule.check(value))
      .map((item) => typeof item.message === 'function' ? item.message() : item.message)
  }
}
export type Rule = {
  check: (value?: Field['value']) => boolean;
  message: string|(() => string);
}
export function isPersonName (value?: Field['value']): boolean {
  if (typeof value !== 'string') {
    return false
  }
  return /^[a-zA-Zа-яА-ЯЁё ]{2,60}$/.test(value)
}
export function isNotEmpty (value?: Field['value']): boolean {
  if (typeof value !== 'string' && !Array.isArray(value)) {
    return value != null
  }
  return value != null && value.length > 0
}
export function isEmail (value?: Field['value']) {
  if (typeof value !== 'string') {
    return false
  }
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(value.toLocaleLowerCase())
}
