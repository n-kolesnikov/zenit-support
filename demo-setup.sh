#!/bin/bash
cd /var/www/zenit/zenit-support
docker-compose stop
git reset --hard
git pull origin master
docker-compose up -d
